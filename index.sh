# All repos of interest
readonly slugs=$(cat <<!
germs-dev/concurrency-support-library
germs-dev/cpp
germs-dev/fix
germs-dev/cs

germs-dev/hear
germs-dev/pjsip
germs-dev/openai
germs-dev/scripts

germs-dev/deanturpin
germs-dev/webmeup
germs-dev/tracehost
germs-dev/energy

germs-dev/quotations
germs-dev/skills
germs-dev/render
germs-dev/mews-one

germs-dev/brighton
deanturpin/deanturpin
deanturpin/the-brighton-watch
deanturpin/idrawhouses

deanturpin/recipes2
germs-dev/dft
germs-dev/bt
deanturpin/backtest

deanturpin/ev
deanturpin/constd
deanturpin/cmake-external-project
!
)

# Add page info and timestamp
cat index.md
echo
date
echo

# Badges
for slug in $slugs; do
	echo "[![$slug](https://gitlab.com/$slug/badges/main/pipeline.svg)](https://gitlab.com/$slug/-/pipelines) "
done
