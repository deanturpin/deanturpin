# TURPIN.DEV

- [GitLab](https://gitlab.com/deanturpin)
- [GitHub](https://github.com/deanturpin)
- [LinkedIn](https://www.linkedin.com/in/deanturpin)
- [Docker hub](https://hub.docker.com/u/deanturpin)

## Read

- [The Climate Book](https://www.amazon.co.uk/gp/product/0241547474/) -- Greta Thunberg
- [The Art of Writing Efficient Programs](https://www.amazon.co.uk/Art-Writing-Efficient-Programs-optimizations/dp/1800208111/) -- Fedor G. Pikus

## Listen

<iframe width="100%" height="120" src="https://player-widget.mixcloud.com/widget/iframe/?hide_cover=1&feed=%2Fdeanturbeaux%2Fslow-horses-1%2F" frameborder="0" ></iframe>

## Code

| Title | Description |
|-|-|
| [deanturpin/shh](https://hub.docker.com/r/deanturpin/shh) | Command line packet inspector |
| [deanturpin/gcc](https://hub.docker.com/r/deanturpin/gcc) | Nightly gcc container build from source |
| [deanturpin/dev](https://hub.docker.com/r/deanturpin/dev) | C++ dev container |
| [constd](https://deanturpin.gitlab.io/constd/) | `constexpr` Standard Library functions |
| [Trading backtester (stocks)](https://deanturpin.gitlab.io/backtest/) | Backtester 2024 (modules and constexpr) |
| [Trading backtester (crypto)](https://cpp.run/) | Backtester from scratch |
| [std::thread](https://germs-dev.gitlab.io/concurrency-support-library/) | C++ Concurrency Support Library |
| [The Plotting Parlour](https://germs-dev.gitlab.io/dft/) | Discrete Fourier transforms |
| [C++ recipes](https://germs-dev.gitlab.io/cpp/) | C++ idioms and good practice |
| [Teach yourself C++ in 45 years](https://germs-dev.gitlab.io/cs/) | Survival guide for C++/Linux devs |
| [Online logbook](https://germs.dev/) | Curated since 2009 |
| [The Brighton Watch](https://superdean.com/) | Watchmaking and collector resources |
| [EV myths](https://deanturpin.gitlab.io/ev/) | Common misconceptions |

